package com.anji.sp.mapper;

import com.anji.sp.model.po.SpAppReleasePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * app灰度发布用户信息表
 *
 * @author Kean 2020-06-23
 */
public interface SpAppReleaseMapper extends BaseMapper<SpAppReleasePO> {
}