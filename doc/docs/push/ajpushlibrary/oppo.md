# oppo 

## 开启推送服务  

1、登录oppo开放平台->点击“管理中心”->点击“应用服务平台”->点击侧边栏的“开发服务”、“消息推送”（或点击列表中的应用进入应用详情->点击“推送服务”）,若应用已经存在直接点击应用->点击“申请开通”即可申请开通应用的推送服务，如下图所示：  
![avatar](../../assets/oppo1.png)   
2、若不存在则点击“创建新应用”，填写应用，如下图所示: 
![avatar](../../assets/oppo2.png)
![avatar](../../assets/oppo3.png)  
3、点击列表中的应用进入应用详情->点击“推送服务”->点击“申请开通”即可申请开通应用的推送服务,如下图所示： 
![avatar](../../assets/oppo4.png) 
![avatar](../../assets/oppo5.png)  

## 配置AndroidManifest文件  

在lib包下面添加aroppor包，并在app目录下的build.gradle中添加aar依赖  
![avatar](../../assets/oppo6.png)  
## 应用开发  

### 注册推送服务

``` java
private ICallBackResultService mPushCallback = new ICallBackResultService() {
        @Override
        public void onRegister(int responseCode, String registerID) {
            //注册的结果,如果注册成功,registerID就是客户端的唯一身份标识
            if (responseCode == 0) {
                AppParam.pushToken = registerID;
                AppParam.brandType = AppParam.OPPO;
                //将获取的token上传给服务器
                AppSpConfig.getInstance().sendRegTokenToServer(new IAppSpCallback() {
                    @Override
                    public void pushInfo(AppSpModel<String> appSpModel) {

                    }

                    @Override
                    public void error(String code, String msg) {

                    }
                });
                AppSpLog.e("注册成功, registerId:" + registerID);
            } else {
                AppSpLog.e("注册失败," + "code=" + responseCode + ",msg=" + registerID);
            }
        }

        @Override
        public void onUnRegister(int code) {
            //反注册的结果
            if (code == 0) {
                AppSpLog.e("注销成功, code:" + code);
            } else {
                AppSpLog.e("注销失败, code:" + code);
            }
        }

        @Override
        public void onGetPushStatus(final int code, int status) {
            //获取当前的push状态返回,根据返回码判断当前的push状态
            if (code == 0 && status == 0) {
                AppSpLog.e("Push状态正常," + "code=" + code + ",status=" + status);
            } else {
                AppSpLog.e("Push状态错误," + "code=" + code + ",status=" + status);
            }
        }

        @Override
        public void onGetNotificationStatus(final int code, final int status) {
            //获取当前通知栏状态
            if (code == 0 && status == 0) {
                AppSpLog.e("通知状态正常," + "code=" + code + ",status=" + status);
            } else {
                AppSpLog.e("通知状态错误," + "code=" + code + ",status=" + status);
            }
        }

        @Override
        public void onSetPushTime(final int code, final String s) {
            //获取设置推送时间的执行结果
            AppSpLog.e("SetPushTime," + "code=" + code + ",result=" + s);
        }

    };

 @Override
    public void onCreate() {
    //在application的oncreat()方法中注册
    try {
        OppoPushUtil oppoPushUtil = new OppoPushUtil(this);
            oppoPushUtil.init();
            if (oppoPushUtil.isSupportPush()) {
                oppoPushUtil.register(mPushCallback);//setPushCallback接口也可设置callback
                oppoPushUtil.requestNotificationPermission();
            } else {
                Log.i(TAG, "不支持oppo推送服务");
            }
    } catch (Exception e) {
            e.printStackTrace();
        } 
} 
``` 
``` java
public class OppoPushUtil {
    private Context context;

    public OppoPushUtil(Context context) {
        this.context = context;
    }

    //判断是否手机平台是否支持PUSH
    public boolean isSupportPush() {
        return HeytapPushManager.isSupportPush();
    }

    //sdk初始化
    public void init() {
        HeytapPushManager.init(context, true);
    }

    //注册OPPO PUSH推送服务
    public void register(ICallBackResultService mPushCallback) {
        HeytapPushManager.register(context, AppParam.oppoAppKey, AppParam.oppoAppSecret, mPushCallback);//setPushCallback接口也可设置callback

    }

    //解注册OPPO PUSH推送服务
    public void unRegister() {
        HeytapPushManager.unRegister();

    }

    //获取注册OPPO PUSH推送服务的注册ID
    public String getRegisterID() {
        return HeytapPushManager.getRegisterID();

    }

    // 暂停接收OPPO PUSH服务推送的消息
    public void pausePush() {
        HeytapPushManager.pausePush();
    }

    // 恢复接收OPPO PUSH服务推送的消息，这时服务器会把暂停时期的推送消息重新推送过来
    public void resumePush() {
        HeytapPushManager.resumePush();
    }

    // 弹出通知栏权限弹窗（ColorOS 5.0以上有效）
    public void requestNotificationPermission() {
        HeytapPushManager.requestNotificationPermission();
    }

    //打开通知栏设置界面
    public void openNotificationSettings() {
        HeytapPushManager.openNotificationSettings();
    }

    // 获取通知栏状态，从callbackresultservice回调结果
    public void getNotificationStatus() {
        HeytapPushManager.getNotificationStatus();
    }

}

```   
### 点击通知消息打开自定义页面

1、 在AndroidManifest.xml文件注册被启动的Activity
``` 
        <activity
            android:name=".Main3Activity"
            android:exported="true">
            <intent-filter>
                <!-- action的‘name’值由您自定义 -->
                <action android:name="com.push.demo.internal" />
                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>
```   
2、在自定义的 Main3Activity 中接收数据  
``` java
    public class Main3Activity extends Activity {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.xxx);
           //获取自定义透传参数值
            if (null != intent) {
                String key1 = intent.getStringExtra("key1");
                int kye2 = intent.getIntExtra("key2", -1);
                tv.setText("用户自定义打开的Activity_oppo，Content : " + key1);
            }
        }
    }
``` 

## 推送测试 
 
1、点击“管理中心”->“应用服务平台”->左侧“开发服务”->“消息推送”->应用，如下图所示：
![avatar](../../assets/oppo7.png)  
2、消息推送运营平台，输入通知消息相关内容，如下图所示：  
![avatar](../../assets/oppo8.png)  
![avatar](../../assets/oppo9.png)   




